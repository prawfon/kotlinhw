package dv.spring.kotlin.service

import dv.spring.kotlin.dao.ShoppingCartDao
import dv.spring.kotlin.entity.ShoppingCart
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class ShoppingCartServiceImpl:ShoppingCartService{
    override fun getCustomerByProductName(name: String): List<ShoppingCart> {
        return shoppingCartDao.getCustomerByProductName(name)
    }

    override fun getAllShoppingCartWithPage(page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getAllShoppingCartWithPage(page,pageSize)
    }


    override fun getShoppingCartWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getShoppingCartWithPage(name,page,pageSize)
    }

    override fun getShoppingCartByProductName(name: String): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCartByProductName(name)
    }

    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao

    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCarts()
    }
}