package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Product
import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page

interface ProductDao{
    fun getProducts():List<Product>
    fun getProductByName(name:String): Product?
    fun getProductByPartialName(name: String): List<Product>
    fun getProductByPartialNameDesc(name: String, desc: String): List<Product>
    fun getProductByManuName(name: String): List<Product>
    fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<Product>
    fun save(product: Product): Product
    fun findById(id: Long): Product?

}
