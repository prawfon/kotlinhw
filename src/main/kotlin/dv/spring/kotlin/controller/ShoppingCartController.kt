package dv.spring.kotlin.controller


import dv.spring.kotlin.entity.dto.PageShoppingCartDto
import dv.spring.kotlin.service.ShoppingCartService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class ShoppingCartController {
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService

    @GetMapping("/shoppingcart")
    fun getAllShoppingCart(): ResponseEntity<Any> {
        val shoppingCart = shoppingCartService.getShoppingCarts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShoppingCartDto(shoppingCart))
    }

    @GetMapping("/shoppingCart/all")
    fun getAllShoppingCartWithPage(@RequestParam("page")page:Int,
                                   @RequestParam("pageSize")pageSize: Int):ResponseEntity<Any>{
        val output = shoppingCartService.getAllShoppingCartWithPage(page,pageSize)
        return ResponseEntity.ok(PageShoppingCartDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                shoppingCarts = MapperUtil.INSTANCE.mapShoppingCartDto(output.content)))
    }

    @GetMapping("/shoppingCart/{productName}")
    fun getShoppingCartByProductName(@PathVariable("productName") name: String): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapShoppingCartDto(
                shoppingCartService.getShoppingCartByProductName(name))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/shoppingCart/productName")
    fun getShoppingCartWithPage(@RequestParam("name") name: String,
                                @RequestParam("page") page: Int,
                                @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        val output = shoppingCartService.getShoppingCartWithPage(name,page,pageSize)
        return ResponseEntity.ok(PageShoppingCartDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                shoppingCarts = MapperUtil.INSTANCE.mapShoppingCartDto(output.content)))
    }

}